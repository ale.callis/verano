from django.db import models

# Create your models here.
class TipoDeAlgo(models.Model):
    nombre = models.CharField(max_length=100)

class Algo(models.Model):
    nombre = models.CharField(max_length=100)
    cantidad = models.IntegerField()
    tipo = models.ForeignKey(TipoDeAlgo,on_delete=models.CASCADE)
    creado = models.DateTimeField(auto_now=True)
    foto = models.ImageField(upload_to="fotos",default="default.png")

    
class OtraCosa(models.Model):
    nombre = models.CharField(max_length=100)
    cantidad = models.IntegerField()
    correo = models.EmailField()
    tipo = models.ForeignKey(TipoDeAlgo,on_delete=models.CASCADE)
    creado = models.DateTimeField(auto_now=True)

    def __str__ (self):
        return self.nombre