from django.contrib import admin
from django.urls import path
from . import views
app_name = "cosas"

urlpatterns = [
   path('crear',views.crear,name="crear"),
   path('borrar/<int:id>',views.borrar,name="borrar"),
   path('actualizar/<int:id>',views.actualizar,name="actualizar"),
   path('otra-cosa/crear',views.otra_cosa_create,name="otra_cosa_crear"),
]
