from django.test import TestCase

from .models import TipoDeAlgo

class TipoDeAlgo_test(TestCase):

    def setUp(self):
        TipoDeAlgo.objects.create(nombre="lion")
        TipoDeAlgo.objects.create(nombre="cat")

    def test_crear_tipo_de_Algo(self):
        ta = TipoDeAlgo.objects.create(nombre="Ejemplo")
        ta.save()
        tab = TipoDeAlgo.objects.filter(nombre="Ejemplo")
        self.assertEqual(len(tab),1)

    def test_listar(self):
        lista_ta = TipoDeAlgo.objects.all()
        print(lista_ta)
        self.assertEqual(len(lista_ta),2)
        self.assertEqual(lista_ta[0].nombre,"lion")

