from django import forms

from .models import OtraCosa

class OtraCosaForm(forms.ModelForm):
    class Meta():
        model = OtraCosa
        fields = ('nombre','cantidad','tipo','correo')