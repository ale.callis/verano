from django.shortcuts import render
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required



from .models import TipoDeAlgo, Algo
from .forms import OtraCosaForm

def crear(request):
    if request.POST:
        nombre = request.POST.get('nombre','') 
        cantidad =request.POST.get('cantidad','') 
        tipo =  request.POST.get('tipo','')
        foto = request.FILES.get('foto',False)
        algo = Algo(nombre=nombre,cantidad=cantidad,tipo=TipoDeAlgo.objects.get(pk=tipo))
        if foto:
            algo.foto = foto    
        algo.save()
        messages.add_message(request, messages.INFO, 'Algo fue creado')

    context = {
        'lista_tda' : TipoDeAlgo.objects.all(),
        'lista_algo' : Algo.objects.all()
    }

    return render(request,'crear_cosas.html',context)

@login_required(login_url="/login")
def borrar(request,id):
    algo = Algo.objects.get(pk=id)
    algo.delete()
    messages.add_message(request, messages.INFO, 'Algo fue borrado')
    return HttpResponseRedirect(reverse('cosas:crear'))

@staff_member_required(login_url="/login")
def actualizar(request,id):
    algo = Algo.objects.get(pk=id)
    if request.POST:
        algo.nombre = request.POST.get('nombre','') 
        algo.cantidad =request.POST.get('cantidad','') 
        algo.tipo =  TipoDeAlgo.objects.get(pk=request.POST.get('tipo','') )
        algo.save()
        messages.add_message(request, messages.INFO, '{} fue editado'.format(algo.nombre))
        return HttpResponseRedirect(reverse('cosas:crear'))
    context = {
        'lista_tda' : TipoDeAlgo.objects.all(),
        'algo':algo
    }
    return render(request,'actualizar_cosas.html',context)

def otra_cosa_create(request):
    if request.POST:
        form = OtraCosaForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO, 'Otra cosa fue creada')
            return HttpResponseRedirect(reverse('cosas:crear'))
    else:
        form = OtraCosaForm()
    return render(request, 'otra_cosa_crear.html', {'form': form}) 


