from cosas.models import OtraCosa,TipoDeAlgo
from rest_framework import  serializers




class OtraCosaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = OtraCosa
        fields = ['id','nombre','cantidad','creado','correo']