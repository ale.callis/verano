from rest_framework import routers
from django.contrib import admin
from django.urls import path,include

from .viewsets import OtraCosaViewSet

app_name = "API"

router = routers.DefaultRouter()
router.register(u'cosas',OtraCosaViewSet)

urlpatterns = [
   path('',include(router.urls))
]