from rest_framework import viewsets
from cosas.models import OtraCosa,TipoDeAlgo
from .serializers import  OtraCosaSerializer



class OtraCosaViewSet(viewsets.ModelViewSet):
    queryset = OtraCosa.objects.all()
    serializer_class = OtraCosaSerializer