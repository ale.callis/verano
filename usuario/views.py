from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth import authenticate, logout, login
from django.urls import reverse
from django.http import HttpResponseRedirect



def index(request):
    return render(request,'inicio.html',{})

def crear(request):
    contex = {'titulo':'Creación de usuario','verdadero':True}
    return render(request,'crear.html',contex)

def iniciar_sesion(request):
    if request.POST:
        usuario = request.POST.get('usuario',False)
        contrasenia = request.POST.get('contrasenia',False)
        
        if usuario and contrasenia:
            user = authenticate(request=request,username=usuario,
            password=contrasenia)
            if user is not None:
                login(request,user)
            else:
                messages.add_message(request, messages.INFO, 'Usuario incorrecto')
        else:
            messages.add_message(request, messages.INFO, 'Debes ingresar los datos')

    return render(request,'login.html')

def cerrar_sesion(request):
    logout(request)
    return HttpResponseRedirect(reverse('iniciar_sesion'))

