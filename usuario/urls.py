
from django.contrib import admin
from django.urls import path,include

app_name = 'usuarios'

from . import views
urlpatterns = [
  path('',views.index,name="index"),
  path('crear/',views.crear,name="crear"),
  path('cerrar_sesion',views.cerrar_sesion,name="cerrar_sesion")
]