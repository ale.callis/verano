
from django.contrib import admin
from django.urls import path,include

from django.conf import settings
from django.conf.urls.static import static

from usuario.views import iniciar_sesion 

urlpatterns = [
    path('admin/', admin.site.urls),
    path('usuario/',include('usuario.urls',namespace="usuarios")),
    path('cosas/',include('cosas.urls',namespace="cosas")),
    path('login/',iniciar_sesion,name="iniciar_sesion"),
    path('api',include('API.urls'))
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
